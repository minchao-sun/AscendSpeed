# Copyright (c) 2024, Huawei Technologies, Co., Ltd. All rights reserved.
import os
import importlib

import pytest


def scan_dir(directory):
    py_files = []
    for root, _, files in os.walk(directory):
        for path in files:
            if path.endswith(".py"):
                py_files.append(os.path.join(root, path))
    return py_files


def filter_files(files):
    results = []
    for path in files:
        with open(path, "r", encoding="utf-8") as f:
            content = f.read()
            index = content.find("__all__")
            if index != -1:
                results.append(path[2:-3].replace("/", "."))
    return results


class TestOpImport:
    def test_op_import(self):
        directory = "./mindspeed/ops"
        py_files = scan_dir(directory)
        op_files = filter_files(py_files)
        for module in op_files:
            print(f"import {module} ...", end=" ")
            importlib.import_module(module)
            print("done")
