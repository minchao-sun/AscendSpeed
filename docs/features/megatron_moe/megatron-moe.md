# Megatron mcore MoE 相关特性

## 特性优化

| 特性                                                       | 介绍                                                    |
|----------------------------------------------------------|-------------------------------------------------------|
| 【Prototype】Ascend Megatron MoE 负载感知内存均衡算法                           | [link](megatron-moe-adaptive-recompute-activation.md) |
| 【Prototype】Megatron MoE GMM                                         | [link](megatron-moe-gmm.md)                           |
| 【Prototype】Ascend Megatron MoE Allgather Dispatcher 性能优化 | [link](megatron-moe-allgather-dispatcher.md)          |
| 【Prototype】Ascend Megatron MoE Alltoall Dispatcher 性能优化  | [link](megatron-moe-alltoall-dispatcher.md)           |
